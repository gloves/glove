# Define pid file to enable force stop feature
CATALINA_PID="$CATALINA_BASE/bin/catalina.pid"
# Make tweaks on jvm heap size
JAVA_OPTS="-Xms512m -Xmx2048m"
