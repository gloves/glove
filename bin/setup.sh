#!/bin/bash
#
# Copyright 2017-present Liu Hongyu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# resolve links - $0 may be a softlink
PRG="$0"
RETCODE=0

while [ -h "$PRG" ]; do
	ls=`ls -ld "$PRG"`
	link=`expr "$ls" : '.*-> \(.*\)$'`
	if expr "$link" : '/.*' > /dev/null; then
		PRG="$link"
	else
		PRG=`dirname "$PRG"`/"$link"
	fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set GLOVE_BASE if not already set
[ -z "$GLOVE_BASE" ] && GLOVE_BASE=`cd "$PRGDIR/.." >/dev/null; pwd`

# Source environment
. "$GLOVE_BASE/etc/glove.env"
. "$GLOVE_BASE/bin/commons.sh"

TMPDIR=`mktemp -d`
LOG_FILE="$LOG_HOME/$(basename "$0" .sh)-$(date +%Y-%m-%d).log"
COMPILE_REDIS="N"

#===  FUNCTION  ================================================================
#         NAME:  show_help
#  DESCRIPTION:  Display help information for this script.
# PARAMETER  1:  ---
#===============================================================================
function show_help() {
cat << EOF

Usage: $(basename "$PRG") [-h] [-c] [app1,app2,...]

Setup hap server environment

Options
  -h           Show help
  -c           Force to re-compile redis source code

EOF
}

# --------------------------------------------------------
# Process user arguments
# --------------------------------------------------------
while getopts ch OPT
do
	case "$OPT" in
		c) COMPILE_REDIS="Y";;
		h) show_help; exit 0;;
		?) show_help; exit 1;;
	esac
done
shift $((OPTIND - 1))

if [ $# -gt 1 ]; then
	error_inline "Invalid arguments: $@"
	show_help
	exit 1
fi

PROFILES=$@

info "Checking prerequisites ..."
# --------------------------------------------------------
# Download necessary service components if not exist
# --------------------------------------------------------
if [ ! -d "$REDIS_HOME" ]; then
	echo ""
	warn "Component \"REDIS\" not found. Now downloading automatically..."
	REDIS_PREFIX="redis-$REDIS_VERSION"
	REDIS_TARBALL="$REDIS_PREFIX.tar.gz"
	cd "$TMPDIR"
	
	if ! curl -L# -o "$REDIS_TARBALL" "$REDIS_DOWNLOAD_URL"; then
		error "Failed to download $REDIS_DOWNLOAD_URL"
		RETCODE=1
	else
		echo "Extracting $REDIS_TARBALL ..."
		tar zxvf "$REDIS_TARBALL" -C "$GLOVE_HOME/lib" >> "$LOG_FILE" 2>&1
		echo "Renaming folder ..."
		mv "$GLOVE_HOME/lib/$REDIS_PREFIX" "$REDIS_HOME"
		echo "Cleaning up ..."
		rm -f "$REDIS_TARBALL"
	fi
fi

if [ ! -d "$CATALINA_HOME" ]; then
	echo ""
	warn "Component \"TOMCAT\" not found. Now downloading automatically..."
	CATALINA_PREFIX="apache-tomcat-$CATALINA_VERSION"
	CATALINA_TARBALL="$CATALINA_PREFIX.tar.gz"
	CATALINA_DLADDR=$(resolve_url $CATALINA_DOWNLOAD_URL $CATALINA_FALLBACK_URL)
	cd "$TMPDIR"
	
	if ! curl -L# -o "$CATALINA_TARBALL" "$CATALINA_DLADDR"; then
		error "Failed to download $CATALINA_DLADDR"
		RETCODE=1
	else
		echo "Extracting $CATALINA_TARBALL ..."
		tar zxvf "$CATALINA_TARBALL" -C "$GLOVE_HOME/lib" >> "$LOG_FILE" 2>&1
		echo "Renaming folder ..."
		mv "$GLOVE_HOME/lib/$CATALINA_PREFIX" "$CATALINA_HOME"
		echo "Cleaning up ..."
		rm -f "$CATALINA_TARBALL"
	fi
fi

# --------------------------------------------------------
# Compile redis source code
# --------------------------------------------------------
if [ "$COMPILE_REDIS" == "Y" ] || [ ! -f "$REDIS_HOME/src/redis-server" ]; then
	command_exists gcc || { error "Redis needs \"gcc\" to compile. Please install gcc first."; exit 1; }
	echo "Compiling source code for redis..."
	cd "$REDIS_HOME"
	echo "Cleaning up previous intermediate files ..."
	make distclean >> "$LOG_FILE" 2>&1
	echo "Making redis binaries ..."
	make >> "$LOG_FILE" 2>&1

	if [ $? -eq 0 ]; then
		echo "Redis compile result: $(green_text 'SUCCESS')"
	else
		echo "Redis compile result: $(red_text 'FAILED')"
		RETCODE=1
	fi
fi

[ $RETCODE -ne 0 ] && exit 1

# --------------------------------------------------------
# Perform setup for each profile
# --------------------------------------------------------
info "Performing setup ..."
"$GLOVE_HOME/bin/profile.sh" "setup" $PROFILES; RETCODE=$?

echo ""
if [ $RETCODE -eq 0 ]; then
	echo "Setup result: $(green_text 'SUCCESS')"
else
	echo "Setup result: $(red_text 'FAILED')"
fi 
echo "Check log file at $(underline $LOG_FILE)"

exit $RETCODE
