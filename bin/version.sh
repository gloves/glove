#!/bin/bash
#
# Copyright 2017-present Liu Hongyu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# resolve links - $0 may be a softlink
PRG="$0"
RETCODE=0

while [ -h "$PRG" ]; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirname "$PRG"`/"$link"
    fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set GLOVE_BASE if not already set
[ -z "$GLOVE_BASE" ] && GLOVE_BASE=`cd "$PRGDIR/.." >/dev/null; pwd`

# Source environment
. "$GLOVE_BASE/etc/glove.env"
. "$GLOVE_BASE/bin/commons.sh"

VERSION="$GLOVE_VERSION"
COMMIT_SHA=

#===  FUNCTION  ================================================================
#         NAME:  show_label
#  DESCRIPTION:  Show label with beautiful font.
# PARAMETER  1:  ---
#===============================================================================
function show_label() {
cat << EOF
   ________    ____ _    ________
  / ____/ /   / __ \ |  / / ____/
 / / __/ /   / / / / | / / __/   
/ /_/ / /___/ /_/ /| |/ / /___   
\____/_____/\____/ |___/_____/ // On-Premises
                                                  
EOF
}

#===  FUNCTION  ================================================================
#         NAME:  op_version
#  DESCRIPTION:  Append COMMIT SHA related to version if possible.
# PARAMETER  1:  ---
#===============================================================================
function op_version() {
    if command_exists git && git rev-parse --git-dir > /dev/null 2>&1; then
        COMMIT_SHA=$(git rev-list -n 1 --abbrev-commit $GLOVE_VERSION 2> /dev/null)
        [ $? -eq 0 ] && VERSION="$GLOVE_VERSION+$COMMIT_SHA"
    fi

    tput_green $VERSION
}

#===  FUNCTION  ================================================================
#         NAME:  main
#  DESCRIPTION:  The main entrypoint of this module.
# PARAMETER  x:  See '--help' content for more details
#===============================================================================
function main() {
    show_label
    echo "Glove  Version: `op_version`"
    [ -d "$GLOVE_HOME/lib/tomcat" ] && echo "Tomcat Version: `tput_green $CATALINA_VERSION`"
    [ -d "$GLOVE_HOME/lib/maven"  ] && echo "Maven  Version: `tput_green $M2_VERSION`"
    [ -d "$GLOVE_HOME/lib/redis"  ] && echo "Redis  Version: `tput_green $REDIS_VERSION`"
    return 0
}

# Main entrypoint goes here
main $@
