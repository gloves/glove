#!/bin/bash
#
# Copyright 2017-present Liu Hongyu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# resolve links - $0 may be a softlink
PRG="$0"
RETCODE=0

while [ -h "$PRG" ]; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirname "$PRG"`/"$link"
    fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set GLOVE_BASE if not already set
[ -z "$GLOVE_BASE" ] && GLOVE_BASE=`cd "$PRGDIR/.." >/dev/null; pwd`

# Source environment
. "$GLOVE_BASE/etc/glove.env"
. "$GLOVE_BASE/bin/commons.sh"

# Hold all profiles that user passed in
PROFILES=

#===  FUNCTION  ================================================================
#         NAME:  precheck
#  DESCRIPTION:  Try to set current active profile as default if no profile is 
#                provided. Use 'set' subcommand to set current active profile.
#	     SCOPE:  normal
# PARAMETER  1:  with active flag
# PARAMETER 2-n: profile list
#===============================================================================
function precheck() {
    local with_active="$1"; shift
    PROFILES=`echo $@ | tr ',' '\n'`

    if [ $# -lt 1 ]; then
        if [ "$with_active" = "with-active" ] && [ -f "$GLOVE_HOME/.profile" ]; then
            PROFILES=$(cat "$GLOVE_HOME/.profile")
        else
            error "One or more profile needs to be provided."
            return 1
        fi
    fi
}

#===  FUNCTION  ================================================================
#         NAME:  validate
#  DESCRIPTION:  Check if user provided PROFILES are valid.
#	     SCOPE:  normal
# PARAMETER  1:  profile list
#===============================================================================
function validate() {
    local retcode=0

    precheck no-active $@; [ $? -ne 0 ] && exit 1

	for profile in $PROFILES; do
		if [ ! -d "$INST_HOME/$profile" ]; then
            error "Profile \"$profile\" does not have home directory. Try \"init\" first."
            retcode=1
            continue
        fi

        if [ ! -f "$INST_HOME/$profile/app.conf" ]; then
            error "Profile \"$profile\" does not have the main config file."
            retcode=1
            continue
        fi

        if cat "$INST_HOME/$profile/app.conf" | grep '###template' >/dev/null; then
            error "It seems that your main config file 'app.conf' have not been edited. "
            error "To do this, please remove THE FIRST TWO LINES in your config file and try again."
            retcode=1
            continue
        fi
	done

    return $retcode
}

#===  FUNCTION  ================================================================
#         NAME:  confirm_with_user
#  DESCRIPTION:  Prompt user if user will proceed the operation.
#	     SCOPE:  normal
# PARAMETER  1:  confirm message
# PARAMETER  2:  refused message
#===============================================================================
function confirm_with_user() {
    local YN="Y"
    local retcode=0
    local confirm_text=`confirm_text "$1"`
    # Give user another chance to determine to continue or not.
    while [ true ]; do
        read -p "$confirm_text [Y] " YN
        if [[ -z $YN ]] || [[ $YN =~ [Yy] ]] ; then
            break
        elif [[ $YN =~ [Nn] ]] ; then
            info "${2}"
            retcode=1
            break
        fi
    done
    return $retcode
}

#===  FUNCTION  ================================================================
#         NAME:  init
#  DESCRIPTION:  Initialize specified profile's home directory
#	     SCOPE:  normal
# PARAMETER  1:  profile list
#===============================================================================
function init() {
    local retcode=0
    local profile_home
    
    precheck no-active $@; [ $? -ne 0 ] && exit 1

	for profile in $PROFILES; do
        profile_home="$INST_HOME/$profile"

        if [ -d "$profile_home" ]; then
            confirm_with_user \
                "Profile \"$profile\" already existed. Override current settings?" \
                "Nothing to do with profile $profile ..."
            if [ $? -ne 0 ]; then
                retcode=1
                continue
            fi
        fi

        info "[`cyan_text $profile`] Making directories ..."
        mkdir -p "$profile_home/backups" \
                 "$profile_home/redis" \
	             "$profile_home/webapps" \
	             "$profile_home/tomcat/bin" \
                 "$profile_home/tomcat/logs" \
                 "$profile_home/tomcat/temp" \
                 "$profile_home/tomcat/webapps" \
                 "$profile_home/tomcat/work"
        
        if [ ! -f "$profile_home/app.conf" ]; then
            \cp -f  "$TEMPLATE_HOME/app.conf" "$profile_home/"
        fi

        info "[`cyan_text $profile`] Successfully created at \"$profile_home\""
    done

    return $retcode
}

#===  FUNCTION  ================================================================
#         NAME:  build_context
#  DESCRIPTION:  Build database context for context.xml
#	     SCOPE:  normal
# PARAMETER  1:  None
#===============================================================================
function build_context() {
    if [[ $db_type = "mysql" ]]; then
        driver_class_name="com.mysql.jdbc.Driver"
        db_url="jdbc:mysql://$db_host:$db_port/$db_schema"
    elif [[ $db_type = "oracle" ]]; then
        driver_class_name="oracle.jdbc.driver.OracleDriver"
        if $db_use_sid; then
            db_url="jdbc:oracle:thin:@$db_host:$db_port:$db_schema"
        else
            db_url="jdbc:oracle:thin:@$db_host:$db_port/$db_schema"
        fi
    fi
    
    jndi_name="jdbc/$db_jndi_name"
    db_password="$(_escape $db_password)"

    cat "$TEMPLATE_HOME/context.xml" \
        | sed 's~{{jndi_name}}~'"$jndi_name"'~g' \
        | sed 's~{{driver_class_name}}~'"$driver_class_name"'~g' \
        | sed 's~{{db_url}}~'"$db_url"'~g' \
        | sed 's~{{db_username}}~'"$db_username"'~g' \
        | sed 's~{{db_password}}~'"$db_password"'~g'
}

#===  FUNCTION  ================================================================
#         NAME:  set_active
#  DESCRIPTION:  Set given profile as default one
#	     SCOPE:  normal
# PARAMETER  1:  profile name
#===============================================================================
function set_active() {
    if validate "$1"; then
        echo "$1" > "$GLOVE_HOME/.profile"
        success "Profile '$1' set as active profile."
    fi
}

#===  FUNCTION  ================================================================
#         NAME:  get_active
#  DESCRIPTION:  Get current active profile
#	     SCOPE:  normal
# PARAMETER  1:  profile name
#===============================================================================
function get_active() {
    local profile_path="$GLOVE_HOME/.profile"
    if [ -f "$profile_path" ]; then
        cat "$profile_path"
    fi
}

#===  FUNCTION  ================================================================
#         NAME:  setup
#  DESCRIPTION:  Initialize specified profile's home directory
#	     SCOPE:  normal
# PARAMETER  1:  profile list
#===============================================================================
function setup() {
    local retcode=0
    
    precheck no-active $@; [ $? -ne 0 ] && exit 1

    # --------------------------------------------------------
    # Setups for each app instance
    # --------------------------------------------------------
    for profile in $PROFILES; do
        CATALINA_BASE="$INST_HOME/$profile/tomcat"

        if validate $profile; then
            info "[`cyan_text $profile`] Reading configuration file ..."
            eval `$GLOVE_HOME/bin/readconfig.py "$INST_HOME/$profile/app.conf"`
            web_wardir=`echo $web_warfile | sed -n 's/\.war//p'`

            # Copying standard config files
            info "[`cyan_text $profile`] Copying conf directory from \$CATALINA_HOME ..."
            \cp -rf "$CATALINA_HOME/conf" "$CATALINA_BASE"

            info "[`cyan_text $profile`] Configuring tomcat's server.xml ..."
            cat "$TEMPLATE_HOME/server.xml" \
                | sed 's~{{web_server_port}}~'$web_server_port'~g' \
                | sed 's~{{web_connector_port_http}}~'$web_connector_port_http'~g' \
                | sed 's~{{web_connector_port_ajp}}~'$web_connector_port_ajp'~g' \
                | sed 's~{{web_connector_port_redirect}}~'$web_connector_port_redirect'~g' \
                > "$CATALINA_BASE/conf/server.xml"

            info "[`cyan_text $profile`] Configuring tomcat's context.xml ..."
            build_context > "$CATALINA_BASE/conf/context.xml"
            
            info "[`cyan_text $profile`] Configuring tomcat's other resources ..."
            cp "$GLOVE_HOME/etc/setenv.sh" "$CATALINA_BASE/bin"
            cp "$GLOVE_HOME/etc/catalina.properties" "$CATALINA_BASE/conf/"

            info "[`cyan_text $profile`] Removing all soft links of old deployments..."
            cd "$CATALINA_BASE/webapps"
            # Remove all soft links
            lnfiles=`find . -type l -name '*'`
            if [ ! -z "$lnfiles" ]; then
                echo "$lnfiles" | xargs rm
            fi

            info "[`cyan_text $profile`] Linking app ..."
            cd "$CATALINA_BASE/webapps"
            ln -s "../../webapps/$web_wardir" "$web_app_name"

            info "[`cyan_text $profile`] Configuring redis ... "
            cat "$TEMPLATE_HOME/redis.conf" \
                | sed 's~{{redis_port}}~'$redis_port'~g' \
                | sed 's~{{bind_addr}}~'$redis_bind_addr'~g' \
                | sed 's~{{dumpdir}}~'"$INST_HOME/$profile/redis"'~g' \
                | sed 's~{{logfile}}~'"$INST_HOME/$profile/redis/redis-$redis_port.log"'~g' \
                | sed 's~{{pidfile}}~'"$INST_HOME/$profile/redis/redis-$redis_port.pid"'~g' \
                > "$INST_HOME/$profile/redis/redis-$redis_port.conf"
            
            if [[ -n $redis_password ]]; then
                redis_password='$(_escape $redis_password)'
                if [[ `uname` == "Darwin" ]]; then
                    sed -i~ \
                        's~^# requirepass {{redis_password}}~requirepass '"$redis_password"'~g' \
                        "$INST_HOME/$profile/redis/redis-$redis_port.conf"
                else
                    sed -i \
                        's~^# requirepass {{redis_password}}~requirepass '"$redis_password"'~g' \
                        "$INST_HOME/$profile/redis/redis-$redis_port.conf"
                fi
            fi
        else
            retcode=1
        fi
    done

    return $retcode
}

#===  FUNCTION  ================================================================
#         NAME:  shutdown
#  DESCRIPTION:  Shutdown profile instances. If -f option provided, the program
#                will attempt to force tomcat to shutdown immediately. 
# PARAMETER  1:  profile list separated with comma
#===============================================================================
function shutdown() {
    local retcode=0
    local catalina_opts=""
    
    # Process options
    while getopts f OPT
    do
        case "$OPT" in
            f) catalina_opts="-force";;
        esac
    done
    shift $((OPTIND - 1))

    # Parse profile list
    precheck with-active $@; [ $? -ne 0 ] && exit 1

    # Perform shutdown operation for each profile
    for profile in $PROFILES; do
        export CATALINA_BASE="$INST_HOME/$profile/tomcat"
        export CATALINA_PID="$CATALINA_BASE/bin/catalina.pid"

        if validate $profile; then
            info "[`cyan_text $profile`] Reading configuration file ..."
            eval `$GLOVE_HOME/bin/readconfig.py "$INST_HOME/$profile/app.conf"`

            info "[`cyan_text $profile`] Shutting down tomcat server ..."
            "$CATALINA_HOME/bin/shutdown.sh" "$catalina_opts"

            if [ $? -ne 0 ] && [ -f "$CATALINA_PID" ]; then
                retcode=1
                error "[$profile] Tomcat did NOT stop in time! (pid=`cat \"$CATALINA_PID\"`))"
                continue
            fi

            info "[`cyan_text $profile`] Shutting down redis server at port $redis_port ..."
	        "$REDIS_HOME/src/redis-cli" -p $redis_port shutdown save
            [ $? -ne 0 ] && retcode=2
        else
            retcode=3
        fi
    done

    return $retcode
}

#===  FUNCTION  ================================================================
#         NAME:  startup
#  DESCRIPTION:  Start profile instances
# PARAMETER  1:  profile list separated with comma
#===============================================================================
function startup() {
    local retcode=0
    
    precheck with-active $@; [ $? -ne 0 ] && exit 1

    # --------------------------------------------------------
    # Setups for each app instance
    # --------------------------------------------------------
    for profile in $PROFILES; do
        export CATALINA_BASE="$INST_HOME/$profile/tomcat"
        export CATALINA_PID="$CATALINA_BASE/bin/catalina.pid"

        if [ -f "$CATALINA_PID" ] && process_exists "$CATALINA_PID"; then
            error "[$profile] Tomcat process is still running. Please try to stop it first."
            retcode=1
            continue
        fi

        if validate $profile; then
            info "[`cyan_text $profile`] Reading configuration file ..."
            eval `$GLOVE_HOME/bin/readconfig.py "$INST_HOME/$profile/app.conf"`

            web_wardir=`echo $web_warfile | sed -n 's/\.war//p'`
            backup_dir="$INST_HOME/$profile/backups"

            info "[`cyan_text $profile`] Starting redis server at port $redis_port ..."
            "$REDIS_HOME/src/redis-server" "$INST_HOME/$profile/redis/redis-$redis_port.conf"

            if [ -f "$INST_HOME/$profile/webapps/$web_warfile" ]; then
                
                cd "$INST_HOME/$profile/webapps"

                if [ -d "$web_wardir" ]; then
                    info "[`cyan_text $profile`] Performing backup ..."
                    [ -f "$web_wardir-backup.war" ] && rm "$web_wardir-backup.war"
                    cd "$web_wardir"
                    zip -rq "$backup_dir/$web_wardir-backup.war" *
                    cd ..
                    rm -rf "$web_wardir"
                fi

                info "[`cyan_text $profile`] Deploying '$web_warfile' ..."
                unzip -q "$web_warfile" -d "$web_wardir"
                rm -rf "$web_warfile"
            fi

            info "[`cyan_text $profile`] Starting tomcat server ..."
            "$CATALINA_HOME/bin/startup.sh"

            info "[`cyan_text $profile`] Homepage: `underline "http://localhost:$web_connector_port_http/$web_app_name/"`"
        else
            retcode=3
        fi
    done

    return $retcode
}

#===  FUNCTION  ================================================================
#         NAME:  restart
#  DESCRIPTION:  Restart profile instances.
# PARAMETER  1:  profile list separated with comma
#===============================================================================
function restart() {
    local retcode=0
    local pretcode
    local force_opt=

    # process force option
    if [ "$1" == '-f' ]; then
        force_opt=$1
        shift
    fi
    
    # try to set current active profile as default if no profile is provided
    precheck with-active $@; [ $? -ne 0 ] && exit 1

    for profile in $PROFILES; do
        pretcode=0
        if validate $profile; then
            if is_running $profile; then
                shutdown $force_opt $profile
                pretcode=$?
                if [ $pretcode -eq 0 ]; then
                    startup $profile
                    pretcode=$?
                fi
            else
                startup $profile
                pretcode=$?
            fi
        else
            pretcode=3
        fi

        # update final return status
        if [ $pretcode -ne 0 ]; then
            retcode=$pretcode
            error "Profile '$profile' failed to restart."
        fi
    done

    if [ $retcode -eq 0 ]; then
        success "Restart finished."
    else
        error "Restart failed."
    fi
    return $retcode
}

#===  FUNCTION  ================================================================
#         NAME:  watchlog
#  DESCRIPTION:  Keep watching tomcat logs
# PARAMETER  1:  profile name
#===============================================================================
function watchlog() {
    precheck with-active $@; [ $? -ne 0 ] && exit 1

    local profile=${1:-$PROFILES}
    local logfile="catalina.out"
    CATALINA_BASE="$INST_HOME/$profile/tomcat"

    if validate $profile; then
        [ -f "$CATALINA_BASE/logs/catalina.$(date +%Y-%m-%d).out" ] && logfile="catalina.$(date +%Y-%m-%d).out"
        tail -f -n $NUM_OF_LINES_TO_LOG "$CATALINA_BASE/logs/$logfile"
    fi
}

#===  FUNCTION  ================================================================
#         NAME:  is_running
#  DESCRIPTION:  Check if current instance is still running
# PARAMETER  1:  pid file path
#===============================================================================
function is_running() {
    local profile=$1

    # Initializing app context
    eval `$GLOVE_HOME/bin/readconfig.py "$INST_HOME/$profile/app.conf"`

    CATALINA_PID="$INST_HOME/$profile/tomcat/bin/catalina.pid"
    REDIS_PID="$INST_HOME/$profile/redis/redis-$redis_port.pid"

    process_exists "$CATALINA_PID" || process_exists "$REDIS_PID"
}

#===  FUNCTION  ================================================================
#         NAME:  component_status
#  DESCRIPTION:  Get status of component
# PARAMETER  1:  pid file path
#===============================================================================
function component_status() {
    if process_exists "$1"; then
        tput_green "Running"
    else
        tput_tan "Stopped"
    fi
}

#===  FUNCTION  ================================================================
#         NAME:  status
#  DESCRIPTION:  Detect component status of specified profile
# PARAMETER  1:  profile name
#===============================================================================
function status() {
    precheck with-active $@; [ $? -ne 0 ] && exit 1
    
    local retcode=0
    local profile=${1:-$PROFILES}

    if validate $profile; then
        # Initializing app context
        eval `$GLOVE_HOME/bin/readconfig.py "$INST_HOME/$profile/app.conf"`
        
        echo "-----------------------"
        echo "Component   Status"
        echo "-----------------------"
        echo "Tomcat      $(component_status "$INST_HOME/$profile/tomcat/bin/catalina.pid")"
        echo "Redis       $(component_status "$INST_HOME/$profile/redis/redis-$redis_port.pid")"
        echo "-----------------------"
        echo ""
    fi
}

#===  FUNCTION  ================================================================
#         NAME:  list
#  DESCRIPTION:  List profiles
# PARAMETER  1:  ---
#===============================================================================
function list() {
    local retcode="0"
    profiles_not_exist="true"
    for profile in `ls -l "$INST_HOME" | grep -E '^d' | awk '{print $NF}'`; do
        profiles_not_exist="false"

        if validate $profile >/dev/null; then
            tput_green "$profile"
        else
            tput_red "$profile"
        fi
    done

    if $profiles_not_exist; then
        warn "No profile found."
        retcode="1"
    fi

    return $retcode
}

#===  FUNCTION  ================================================================
#         NAME:  edit
#  DESCRIPTION:  Edit app.conf of specified instance
# PARAMETER  1:  profile/instance name
#===============================================================================
function edit() {
    precheck with-active $@; [ $? -ne 0 ] && exit 1

    local inst=${1:-$PROFILES}

    if [ ! -f "$INST_HOME/$inst/app.conf" ]; then
        error "Config file not found for \"$inst\""
        exit 1
    fi

    if ! command_exists vi; then
        error "VI editor not found."
        exit 1
    fi
    
    vi "$INST_HOME/$inst/app.conf"
}

#===  FUNCTION  ================================================================
#         NAME:  show_help
#  DESCRIPTION:  Display help information for this script.
# PARAMETER  1:  ---
#===============================================================================
function show_help() {
cat << EOF

Usage: $(basename "$PRG") COMMAND [ARGS]

Commands:
  edit          Edit app.conf
  init          Initialize profile's home directory
  list          List profiles
  logs          Keep watching tomcat logs
  restart       Restart profile instances.
  set           Set specific profile as default
  setup         Setup profile according to app.conf
  start         Start profile instances.
  status        View component status of a profile/instance
  stop          Shutdown profile instances.
  --show-active Get current active profile
  -h|--help     Show help

EOF
}

#===  FUNCTION  ================================================================
#         NAME:  main
#  DESCRIPTION:  The main entrypoint of this module.
# PARAMETER  x:  See '--help' content for more details
#===============================================================================
function main() {
    case "$1" in
        edit)     shift; user_exit edit $@ ;;
        init)     shift; user_exit init $@ ;;
        list)     shift; user_exit list $@ ;;
        logs)     shift; user_exit watchlog $@ ;;
        restart)  shift; user_exit restart $@ ;;
        set)      shift; user_exit set_active $@ ;;
        setup)    shift; user_exit setup $@ ;;
        start)    shift; user_exit startup $@ ;;
        status)   shift; user_exit status $@ ;;
        stop)     shift; user_exit shutdown $@ ;;
        --show-active) shift; user_exit get_active ;;
        -h|--help) show_help ;;
        *) error "Invalid command."; show_help; exit 1; 
    esac
}

# Main entry goes here
main $@
