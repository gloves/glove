#!/bin/bash
#
# Copyright 2017-present Liu Hongyu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# resolve links - $0 may be a softlink
PRG="$0"
RETCODE=0

while [ -h "$PRG" ]; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirname "$PRG"`/"$link"
    fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set GLOVE_BASE if not already set
[ -z "$GLOVE_BASE" ] && GLOVE_BASE=`cd "$PRGDIR/.." >/dev/null; pwd`

# Source environment
. "$GLOVE_BASE/etc/glove.env"
. "$GLOVE_BASE/bin/commons.sh"

#===  FUNCTION  ================================================================
#         NAME:  upgrade
#  DESCRIPTION:  Upgrade Glove by pulling latest update from current branch
# PARAMETER  1:  ---
#===============================================================================
function upgrade() {
    if ! command_exists git; then
        error "Git not installed!"
        exit 1
    fi

    if [ ! -d "$GLOVE_HOME/.git" ]; then
        error "Upgrade is supported only when you install Glove via Git!"
        exit 1
    fi

    git pull
    return $?
}

#===  FUNCTION  ================================================================
#         NAME:  show_help
#  DESCRIPTION:  Display help information for this script.
# PARAMETER  1:  return code, default value is 0
#===============================================================================
function show_help() {
    local retcode=${1:-0}
    cat << EOF

Usage: $(basename "$PRG") COMMAND [ARGS]

Commands:
  ci           Continuous Ingegration support
  init         Intialize profile's home directory
  logs         Follow tomcat main logs (catalina.out)
  profile      Manage profiles
  restart      Restart server
  setup        Setup server
  start        Start server
  status       View component status of a profile/instance
  stop         Stop server. Use '-f' option to instruct tomcat forced to stop
               if it cannot be shutdown normally in 5 secs by default.
  upgrade      Upgrade Glove
  -v|--version Show version info
  -h|--help    Show help

EOF
    return $retcode
}

#===  FUNCTION  ================================================================
#         NAME:  main
#  DESCRIPTION:  The main entrypoint of the Glove.
# PARAMETER  x:  See '--help' content for more details
#===============================================================================
function main() {
    case "$1" in
        ci)      shift; user_exit "$GLOVE_HOME/bin/ci.sh" $@ ;;
        logs)    shift; user_exit "$GLOVE_HOME/bin/profile.sh" "logs" $@ ;;
        init)    shift; user_exit "$GLOVE_HOME/bin/profile.sh" "init" $@ ;;
        inst|instances|instance|profile|profiles) 
                 shift; user_exit "$GLOVE_HOME/bin/profile.sh" $@ ;;
        restart) shift; user_exit "$GLOVE_HOME/bin/profile.sh" "restart" $@ ;;
        setup)   shift; user_exit "$GLOVE_HOME/bin/setup.sh" $@ ;;
        start)   shift; user_exit "$GLOVE_HOME/bin/profile.sh" "start" $@ ;;
        status)  shift; user_exit "$GLOVE_HOME/bin/profile.sh" "status" $@ ;;
        stop)    shift; user_exit "$GLOVE_HOME/bin/profile.sh" "stop" $@ ;;
        upgrade) shift; user_exit upgrade ;;
        -v|--version) shift; user_exit "$GLOVE_HOME/bin/version.sh" ;;
        -h|--help) user_exit show_help ;;
        *) error "Invalid command."; user_exit show_help 1; 
    esac
}

# Main entry goes here
main $@
