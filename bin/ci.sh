#!/bin/bash
#
# Copyright 2017-present Liu Hongyu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# resolve links - $0 may be a softlink
PRG="$0"
RETCODE=0

while [ -h "$PRG" ]; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirname "$PRG"`/"$link"
    fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set GLOVE_BASE if not already set
[ -z "$GLOVE_BASE" ] && GLOVE_BASE=`cd "$PRGDIR/.." >/dev/null; pwd`

# Source environment
. "$GLOVE_BASE/etc/glove.env"
. "$GLOVE_BASE/bin/commons.sh"

# Get current active profile
PROFILE="$("$GLOVE_HOME/bin/profile.sh" --show-active)"

if [ -z "$PROFILE" ]; then
    error "You must set a valid active profile in order to use ci function."
    exit 1
fi

PROFILE_HOME="$INST_HOME/$PROFILE"
LOG_FILE="$LOG_HOME/$(basename "$0" .sh)-$PROFILE-$(date +%Y-%m-%d).log"

#===  FUNCTION  ================================================================
#         NAME:  check_packages
#  DESCRIPTION:  Check depending packages and install it if possible.
#	     SCOPE:  normal
# PARAMETER  1:  ---
#===============================================================================
function check_packages() {
    local retcode=0

    # check maven
    if [ ! -d "$M2_HOME" ]; then
        echo ""
        warn "Component 'MAVEN' not found. Now downloading automatically..."
        TMPDIR="$(mktemp -d)"
        M2_PREFIX="apache-maven-$M2_VERSION"
        M2_TARBALL="$M2_PREFIX.tar.gz"
        M2_DLADDR="$(resolve_url $M2_DOWNLOAD_URL $M2_FALLBACK_URL)"

        if ! curl -L# -o "$TMPDIR/$M2_TARBALL" "$M2_DLADDR"; then
            error "Failed to download $M2_DLADDR"
            retcode=1
        else
            echo "Extracting $M2_TARBALL ..."
            tar zxvf "$TMPDIR/$M2_TARBALL" -C "$GLOVE_HOME/lib" >> "$LOG_FILE" 2>&1
            echo "Renaming folder ..."
            mv "$GLOVE_HOME/lib/$M2_PREFIX" "$M2_HOME"
            echo "Cleaning up ..."
            rm -f "$TMPDIR/$M2_TARBALL"
        fi
    fi

    # check git
    if ! command_exists git; then
        error "Git not installed."
        info "Git is not managed by Glove and you need to install it manually."
        info "Here is some tips for installing git:"
        info "1. $ sudo yum install git"
        info "2. Install via git2u (Recommended):"
        info "   $ sudo yum install https://centos7.iuscommunity.org/ius-release.rpm"
        info "   $ sudo yum install epel-release "
        info "   $ sudo yum install git2u"
        retcode=1
    fi

    return $retcode
}

#===  FUNCTION  ================================================================
#         NAME:  set_repo
#  DESCRIPTION:  Clone source code from git repo under current instance.
#	     SCOPE:  normal
# PARAMETER  1:  git repo url
#===============================================================================
function set_repo() {
    local repo_url="$1"

    if [ -z "$repo_url" ]; then
        error "Repo URL must be provided."
        exit 1
    fi

    # Check and install depending packages properly
    check_packages; [ $? -ne 0 ] && exit 1

    if [ ! -d "$PROFILE_HOME/src" ]; then
        info "[`cyan_text $PROFILE`] Cloning repo ..."
        git clone "$repo_url" "$PROFILE_HOME/src"
    else
        info "[`cyan_text $PROFILE`] It seems you've already setup repo ..."
    fi
}

#===  FUNCTION  ================================================================
#         NAME:  stage_error
#  DESCRIPTION:  Print stage error message
#	     SCOPE:  normal
# PARAMETER  1:  stage
#===============================================================================
function stage_error() {
    error "CI failed at stage '$1'"
    exit 1
}

#===  FUNCTION  ================================================================
#         NAME:  rollout
#  DESCRIPTION:  Clone source code from git repo under current instance.
#	     SCOPE:  normal
# OPTIONAL PARAM 1:  --maven-profile <PROFILE>
# OPTIONAL PARAM 2:  --branch <BRANCH_NAME>
# OPTIONAL PARAM 3:  --force|-f Kill tomcat process if it did not stop in time.
#===============================================================================
function rollout() {
    local branch="master"
    local maven_profile="$PROFILE"
    local force_opt
    local stage

    while [ -n "$1" ]; do
        case "$1" in
            --maven-profile) shift; maven_profile="$1" ;;
            --branch) shift; branch="$1" ;;
            --force|-f) force_opt='-f';
        esac
        shift
    done

    if [ ! -d "$PROFILE_HOME/src" ]; then
        error "You did't setup Git repo yet. Try using 'set-repo' option."
        exit 1
    fi

    cd "$PROFILE_HOME/src"

    info "[`cyan_text $PROFILE`] Log file created at '$LOG_FILE'"
    echo "------------------------------------------------------" >> "$LOG_FILE"
    error "CI Log Details on $(date +'%Y-%m-%dT%H:%M:%S%z')"      >> "$LOG_FILE"
    echo "------------------------------------------------------" >> "$LOG_FILE"

    stage="switching-branch"
    info "[`cyan_text $PROFILE`] Switch to branch $branch ..." | tee -a "$LOG_FILE"
    git checkout $branch >> "$LOG_FILE" 2>&1
    [ $? -ne 0 ] && stage_error $stage
    
    stage="pulling-updates"
    info "[`cyan_text $PROFILE`] Pulling updates from remote branch $branch ..." \
         | tee -a "$LOG_FILE"
    git pull origin $branch >> "$LOG_FILE" 2>&1
    [ $? -ne 0 ] && stage_error $stage

    stage="maven-packaging"
    info "[`cyan_text $PROFILE`] Packaging WAR file with maven profile '$maven_profile'..." \
        | tee -a "$LOG_FILE"
    "$M2_HOME/bin/mvn" clean install -P$maven_profile >> "$LOG_FILE" 2>&1
    [ $? -ne 0 ] && stage_error $stage

    if [ -f "core/target/core.war" ]; then
        stage="copy-war"
        info "[`cyan_text $PROFILE`] Copy WAR to instance $PROFILE ..."  | tee -a "$LOG_FILE"
        \cp -f core/target/core.war "$INST_HOME/$PROFILE/webapps/"
        [ $? -ne 0 ] && stage_error $stage
        
        stage="restart-instance"
        info "[`cyan_text $PROFILE`] Restarting instance $PROFILE ..."  | tee -a "$LOG_FILE"
        "$GLOVE_HOME/bin/glove.sh" restart $force_opt $PROFILE >> "$LOG_FILE" 2>&1
    fi

}

#===  FUNCTION  ================================================================
#         NAME:  show_help
#  DESCRIPTION:  Display help information for this script.
# PARAMETER  1:  ---
#===============================================================================
function show_help() {
cat << EOF

Usage: $(basename "$PRG") COMMAND [ARGS]

Commands:
  set-repo     Set source git repo
  rollout      Trigger CI deployment on current instance
  -h|--help    Show help

EOF
}

#===  FUNCTION  ================================================================
#         NAME:  main
#  DESCRIPTION:  The main entrypoint of the Glove.
# PARAMETER  x:  See '--help' content for more details
#===============================================================================
function main() {
    case "$1" in
        set-repo)  shift; set_repo $@; exit $? ;;
        rollout)   shift; rollout  $@; exit $? ;;
        -h|--help) show_help ;;
        *) error "Invalid command."; show_help; exit 1; 
    esac
}

# Main entrypoint goes here
main $@
