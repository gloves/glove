#!/usr/bin/python
#-*- coding: utf-8 -*-
#
# Copyright 2017-2018 Liu Hongyu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from __future__ import print_function

from ConfigParser import ConfigParser
import sys

# Print shell variable declaration to std output
def put_variable(var, val):
    print("".join((var, "=\"", val, "\"")))

# Parse config file into shell variable definition statement
def parse_config(stream):
    conf = ConfigParser()
    conf.readfp(stream)

    sections = conf.sections()
    
    if "global" in sections:
        sections.remove("global")
    
    for section in sections:
        for (option, value) in conf.items(section):
            put_variable("".join((section, "_", option)), value)

if __name__ == "__main__":
    parse_config(open(sys.argv[1], "r"))