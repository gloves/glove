#!/bin/bash
#
# Copyright 2017-present Liu Hongyu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# resolve links - $0 may be a softlink
PRG="$0"
RETCODE=0

while [ -h "$PRG" ]; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirname "$PRG"`/"$link"
    fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set GLOVE_BASE if not already set
[ -z "$GLOVE_BASE" ] && GLOVE_BASE=`cd "$PRGDIR/.." >/dev/null; pwd`

# Source environment
. "$GLOVE_BASE/etc/glove.env"
. "$GLOVE_BASE/bin/commons.sh"

INST_NAME=test$(date +%Y%m%d%H%M%S)
SLEEP_SECONDS="10s"

"${GLOVE_HOME}/bin/glove.sh" --version
"${GLOVE_HOME}/bin/glove.sh" init ${INST_NAME}

if [[ `uname` == "Darwin" ]]; then
    sed -i~ 's~^###template.*~~g' "${GLOVE_HOME}/inst/${INST_NAME}/app.conf"
    sed -i~ 's~^# Remove.*~~g' "${GLOVE_HOME}/inst/${INST_NAME}/app.conf"
else
    sed -i 's~^###template.*~~g' "${GLOVE_HOME}/inst/${INST_NAME}/app.conf"
    sed -i 's~^# Remove.*~~g' "${GLOVE_HOME}/inst/${INST_NAME}/app.conf"
fi

"${GLOVE_HOME}/bin/glove.sh" setup ${INST_NAME}
"${GLOVE_HOME}/bin/glove.sh" start ${INST_NAME}
echo "Sleep $SLEEP_SECONDS to wait for tomcat up and running ..." 
sleep $SLEEP_SECONDS
"${GLOVE_HOME}/bin/glove.sh" stop -f ${INST_NAME}
echo "Cleaning up ..."
rm -rf "${GLOVE_HOME}/inst/${INST_NAME}"
