#!/bin/bash
#
# Copyright 2017-present Liu Hongyu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# =============================================================================
# Define routines to output content with colors
# Thanks to the main script of vmware harbor
# =============================================================================
# set +e
set -o noglob

#
# Set Colors
#

bold=$(tty -s > /dev/null 2>&1 && tput bold)
underline=$(tty -s > /dev/null 2>&1 && tput sgr 0 1)
reset=$(tty -s > /dev/null 2>&1 && tput sgr0)

red=$(tty -s > /dev/null 2>&1 && tput setaf 1)
green=$(tty -s > /dev/null 2>&1 && tput setaf 76)
white=$(tty -s > /dev/null 2>&1 && tput setaf 7)
tan=$(tty -s > /dev/null 2>&1 && tput setaf 202)
blue=$(tty -s > /dev/null 2>&1 && tput setaf 25)
cyan=$(tty -s > /dev/null 2>&1 && tput setaf 6)

#
# Headers and Logging
#

underline() { printf "${underline}${bold}%s${reset}\n" "$@"
}
h1() { printf "\n${underline}${bold}${blue}%s${reset}\n" "$@"
}
h2() { printf "\n${underline}${bold}${white}%s${reset}\n" "$@"
}
debug() { printf "${white}%s${reset}\n" "$@"
}
info() { printf "${white}➜ %s${reset}\n" "$@"
}
success() { printf "${green}✔ %s${reset}\n" "$@"
}
error() { printf "${red}✖ %s${reset}\n" "$@"
}
warn() { printf "${tan}➜ %s${reset}\n" "$@"
}
bold() { printf "${bold}%s${reset}\n" "$@"
}
note() { printf "\n${underline}${bold}${blue}Note:${reset} ${blue}%s${reset}\n" "$@"
}

# Extra routines
tput_green() { printf "${green}%s${reset}\n" "$@"
}
tput_red() { printf "${red}%s${reset}\n" "$@"
}
tput_tan() { printf "${tan}%s${reset}\n" "$@"
}
error_inline() { echo "${red}✖ $@ ${reset}"
}
confirm_text() { printf "${tan}☞ %s${reset}" "$@"
}

cyan_text() { printf "${cyan}$@${reset}"; }
green_text() { printf "${green}$@${reset}"; }
red_text() { printf "${red}$@${reset}"; }

#set -e
set +o noglob

#===  FUNCTION  ================================================================
#         NAME:  _get
#  DESCRIPTION:  Try to evaluate a environment variable
#	     SCOPE:  private|common
# PARAMETER  1:  Part of the variable name (without app prefix)
#===============================================================================
function _get() {
	echo $(eval "echo \$${APP}_$1")
}

#===  FUNCTION  ================================================================
#         NAME:  resolve_url
#  DESCRIPTION:  Resolve component download url
#	     SCOPE:  private|common
# PARAMETER  1:  original url
# PARAMETER  2:  fallback url
#===============================================================================
function resolve_url() {
    local original_url="$1"
    local fallback_url="$2"
	local http_status_code=$(curl -Is "$original_url" | head -n 1 | awk '{print $2}')

    case "$http_status_code" in
        404) echo $fallback_url ;;
        *)   echo $original_url ;;
    esac
}

#===  FUNCTION  ================================================================
#         NAME:  _escape
#  DESCRIPTION:  Escape sed separator preceding with a backslash
#	     SCOPE:  private|common
# PARAMETER  1:  string
#===============================================================================
function _escape() {
    local s='~'
    echo $1 | sed 's~\'$s'~\\\'$s'~g'
}

#===  FUNCTION  ================================================================
#         NAME:  command_exists
#  DESCRIPTION:  Check if given commands exist in current $PATH
#	     SCOPE:  common
# PARAMETER  n:  commands
#===============================================================================
function command_exists() {
    command -v "$@" > /dev/null 2>&1
}

#===  FUNCTION  ================================================================
#         NAME:  process_exists
#  DESCRIPTION:  Check if process still running using given PID
#	     SCOPE:  common
# PARAMETER  1:  PID or file that contains PID
#===============================================================================
function process_exists() {
    PID="$1"
    [ -f "$PID" ] && PID=$(cat "$PID")
    ps -p $PID > /dev/null 2>&1
}

#===  FUNCTION  ================================================================
#         NAME:  user_exit
#  DESCRIPTION:  Execute a command and exit
#	     SCOPE:  common
# PARAMETER  n:  commands
#===============================================================================
function user_exit() {
    eval $@; exit $?
}
