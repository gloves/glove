# Glove

[![Licensed under Apache License version 2.0](https://img.shields.io/badge/license-Apache%202.0-brighgreen.svg)](https://www.apache.org/licenses/LICENSE-2.0) [![pipeline status](https://gitlab.com/gloves/glove/badges/master/pipeline.svg)](https://gitlab.com/gloves/glove/commits/master)

Glove 是针对「手厂」出品的 J2EE 应用开发平台 **HAP** 下所开发的应用进行便捷的部署和维护。本项目可用于主流 Linux 发行版 和 macOS 环境下快速搭建 HAP 服务器环境并同时提供配置文件、管理脚本等功能。软件架构图描述如下：

![architecture](assets/architecture.png)



## 功能特性

| 功能特性                                     | 最小版本支持 |
| -------------------------------------------- | ------------ |
| 持续集成 CI 功能支持                         | v3.1.8       |
| 增加对 Oracle 数据库连接的支持               | v3.1.2       |
| Tomcat 日志实时监控                          | v3.1.0       |
| 自动备份 web 部署包（服务启动时）            | v3.1.0       |
| 支持通过切换预置文件来实现多个系统间切换管理 | v3.1.0       |
| 多实例并行管理                               | v3.1.0       |
| 自动管理 Tomcat 和 Redis 服务组件            | v1.0.0       |



## 前置要求

1. 推荐使用 `bash` 作为默认的 Shell
2. `git`, `gcc` , `vi` 和 `zip` 需要提前安装在系统中

> **提示：** CentOS 下推荐使用 [git2u](https://stackoverflow.com/questions/21820715/how-to-install-latest-version-of-git-on-centos-7-x-6-x/38133865#38133865) 安装 Git，默认的 yum 仓库中的 Git 版本比较陈旧，很多新的功能特性都不支持。



## 服务组件清单

下表列出了运行 HAP 应用所需的组件清单。其中备注为“需自行安装”的组件，用户需要手工进行安装部署；标注为“可选”的组件为非必需组件，用户可根据情况自行安装配置。未进行特殊注明的组件，均由 Glove 自动托管安装和维护。

| 服务组件名称       | 版本   | 备注                                                         |
| ------------------ | ------ | ------------------------------------------------------------ |
| Apache Tomcat      | 8.5    |                                                              |
| Apache Maven       | 3.6.0  |                                                              |
| Redis              | 5.0.0  |                                                              |
| OpenJDK/Oracle JDK | 1.8+   | *需自行安装*，链接：[http://openjdk.java.net/install/](http://openjdk.java.net/install/) |
| MySQL（可选）      | 5.7.x  | *需自行安装*，链接：[https://dev.mysql.com/downloads/mysql/](https://dev.mysql.com/downloads/mysql/) |
| Oracle（可选）     | 11g+   |                                                              |
| RabbitMQ (可选)    | latest | *需自行安装*，连接：https://www.rabbitmq.com/download.html   |

> 注意：版本标识为 latest 的组件指的是您只需要安装当下官方发布的最新版即可。
>



## 目录和重要文件说明

表格中的 `{app}` 表示应用实例名称。

| 目录/文件                    | 含义                                                   | 备注                                          |
| ---------------------------- | ------------------------------------------------------ | --------------------------------------------- |
| etc/                         | 配置文件所在目录                                       |                                               |
| lib/                         | 服务组件的主目录                                       | 如 Tomcat, Redis 和 Maven                     |
| logs/                        | 日志目录                                               |                                               |
| bin/                         | Glove 服务器管理脚本所在目录                           |                                               |
| [bin/glove.sh](bin/glove.sh) | 主程序脚本，所有管理操作必须调用此命令                 |                                               |
| inst/                        | 应用实例的相关文件                                     |                                               |
| inst/{app}/app.conf          | 应用配置文件                                           | 用户需要修改此配置文件                        |
| inst/{app}/backups/          | 应用实例备份目录                                       | 自动生成目录                                  |
| inst/{app}/redis/            | 应用实例所生成的redis相关文件，如pid、日志和dump文件等 | 自动生成目录                                  |
| inst/{app}/webapps/          | 应用实例的Web部署文件war的存储目录                     | 自动生成目录，*用户需要手工将war包放置在此。* |
| inst/{app}/tomcat/           | tomcat 应用实例目录，即 `$CATALINA_BASE`               |                                               |



## 使用步骤

### 克隆或下载项目

方式1：克隆项目（**推荐**）

```bash
git clone https://gitlab.com/gloves/glove.git
```

方式2：直接下载

用户访问 https://gitlab.com/gloves/glove/tags 获取最新发布的稳定版本进行下载和安装。

例如用户选择 `v3.1.8` 标签，点击下载 Download tar.gz

```bash
curl -Lo /tmp/glove.tar.gz https://gitlab.com/gloves/glove/repository/v3.1.8/archive.tar.gz
sudo tar -zxvf /tmp/glove.tar.gz -C /usr/local/share/
# 解压后得到文件夹 glove-v3.1.8-{COMMIT-SHA}
sudo mv /usr/local/share/glove-v3.1.8-{COMMIT-SHA} /usr/local/share/glove
```



### 初始化应用

使用 `init` 选项对一个或者多个要配置的应用初始化目录结构，初始化完成之后，目录将存在 `inst/{app}/` 下。例如下面的命令将会初始化 dev 和 test  两个应用实例：

```bash
./glove.sh init dev,test
```



### 修改应用配置文件

应用配置文件 `inst/{app}/app.conf` 是用户配置应用的主要文件，包括端口、数据库连接信息、应用部署包信息等等。开发者可以手工修改，也可以使用以下命令自动使用vi打开对应的应用配置文件进行编辑：

```bash
./glove.sh edit [app]
```

> ☞ 注意：保存前请确保配置文件前两行注释已经删除掉。
>
> ```properties
> ###template
> # Remove the line above when you've finished your configurations!!!
> ...
> ```
>
> 如未删除掉，Glove 将会认为用户并未对应用做过实际的修改和调整，请注意！

配置文件中的区域和选项的详细说明见下表：

| 区域  | 选项                    | 说明                                                         |
| ----- | ----------------------- | ------------------------------------------------------------ |
| web   | app_name                | 应用名称，配置成功之后可以以 `http://<host>:<ip>/<app_name>` 访问应用 |
| web   | warfile                 | Web部署包名称，如`core.war`                                  |
| web   | server_port             | 对应 `server.xml` 中的服务器端口                             |
| web   | connector_port_http     | 对应 `server.xml` 中的 HTTP 端口                             |
| web   | connector_port_ajp      | 对应 `server.xml` 中的 AJP 端口                              |
| web   | connector_port_redirect | 对应 `server.xml` 中的 redirectPort 属性                     |
| db    | type                    | 数据源类型，可选值为 `mysql` 和 `oracle`                     |
| db    | jndi_name               | 对应 `context.xml` 下的 JNDI 名称                            |
| db    | host                    | 数据库连接地址                                               |
| db    | port                    | 数据库连接端口号                                             |
| db    | use_sid                 | true: 使用 SID 连接 Oracle 数据库, false: 使用 Service Name 连接 Oracle 数据库；该属性仅在 type 为 oracle 时起作用 |
| db    | schema                  | 数据库的Schema名称                                           |
| db    | username                | 数据库用户名                                                 |
| db    | password                | 数据库密码                                                   |
| redis | port                    | Redis端口号                                                  |
| redis | password                | Redis密码，默认为空                                          |



### 放置 Web 部署包

> **☞ 提示：**推荐使用“**持续集成功能**”

将 `<app>.war` 解压到 `inst/<app>/webapps` 下, 程序将会在启动步骤自动解压部署。请注意WAR 包的名字必须与对应配置文件下的 `app.conf` 文件所列示的 `war_file` 选项的值相匹配。



### 应用配置

运行如下命令对应用实例进行设置

```bash
./glove.sh setup dev,test
```
> 注意：若 `app.conf` 发生变更，则需要再次运行 `setup` 命令才能生效。



### 启动服务

运行以下命令启动 Tomcat 和 Redis 服务：

```bash
./glove.sh start dev,test
```



## 持续集成功能

自 `v3.1.8` 版本起，我们增加了持续集成功能，将源代码的构建和打包从本地变成在服务器端完成。在某些项目场景下，这样做可以减少上传 WAR 包的时间，极大地提高版本发布的效率。本次的持续集成功能的基本实现步骤如下：

1. 设置仓库地址并克隆源代码
2. 执行 `mvn clean install -P<profile>` 进行编译打包
3. 拷贝 WAR 包至**当前活动实例**的主目录下的 webapps 子目录
4. 重启应用实例

接下来我们介绍持续集成功能的基本使用方法。

### 使用方法

1. 设置当前活动的应用实例，例如 dev：

   ```bash
   ./glove.sh profile set dev
   ```

2. 设置源代码仓库地址并克隆源代码：

   ```bash
   ./glove.sh ci set-repo <REPO_URL>
   ```

3. 构建部署，例如我们想要使用master分支的最新代码和名为 sit 的Maven Profile来部署应用：

   ```bash
   ./glove.sh ci rollout --branch master --maven-profile sit
   ```

最后的输出结果大致如下：

```
➜ ./glove.sh ci rollout
➜ [dev] Log file created at '/path/to/glove/logs/ci-dev-2018-10-22.log'
➜ [dev] Pulling updates from remote branch master ...
➜ [dev] Packaging WAR file ...
➜ [dev] Copy WAR to instance dev ...
➜ [dev] Restarting instance dev ...
```

我们在输出结果中可以得到完整的 CI 日志文件，开发者可以检查此文件内容来确认执行过程中是否有错误。



## 常见维护任务场景

进入`bin`目录使用脚本`glove.sh`对服务组件进行统一管理，以下列举一些常见任务场景。

| 任务场景         | 命令                               |
| ---------------- | ---------------------------------- |
| 停止应用实例     | ./glove.sh stop dev,test           |
| 强制停止应用实例 | ./glove.sh stop -f dev,test        |
| 重启应用实例     | ./glove.sh restart dev,test        |
| 查看组件运行状态 | ./glove.sh status <app_name>       |
| 跟踪日志输出     | ./glove.sh logs <app_name>         |
| 编辑应用配置文件 | ./glove.sh profile edit <app_name> |

### 更新应用配置

例如应用 dev 发生了配置变更，则运行

```bash
# 停止服务
./glove.sh stop dev
# 编辑 inst/{app}/app.conf
# 重新配置环境
./glove.sh setup dev
# 启动应用
./glove.sh start dev
```

### 其他命令参考

```bash
./glove.sh --help       # 打印帮助内容
./glove.sh --version    # 打印 Glove 核心版本和服务组件版本
./glove.sh profile list # 输出应用实例清单，绿色代表已配置，红色代表未配置
```